GET = 'get'
HEAD = 'head'
POST = 'post'
PUT = 'put'
DELETE = 'delete'
CONNECT = 'connect'
OPTIONS = 'options'
PATCH = 'patch'
TRACE = 'trace'
