=======
History
=======

2.0.1 (2019-01-03)
------------------

* Readme change to clarify the new API expectations


2.0.0 (Who can remember)
------------------

* API now behaves differently.
  * no more kwargs on endpoint calls, use `query={}` to pass query params and `params={}` to pass post params


1.0.0 (2017-09-13)
------------------

* First release on PyPI.
